import argparse
import os
import xarray
import pandas as pd
import numpy as np
from netCDF4 import Dataset
from main import create_model_by_experiment_path_and_stage
from modules.training_helper import rotation_blending
from modules.image_processor import crop_center
from modules.feature_generator import data_cleaning_and_organizing


def load_input_nc_files(input_folder):
    data_info = pd.DataFrame(columns=['region', 'ID', 'lon', 'lat', 'time', 'Vmax', 'R35_4qAVG', 'MSLP'])
    data_matrix = []

    nc_files = [f for f in os.listdir(input_folder) if f.endswith('.nc')]
    for nc_file in nc_files:
        file_path = os.path.join(input_folder, nc_file)
        frame_data = xarray.open_dataset(file_path)

        region = frame_data.region
        ID = frame_data.ID
        time = frame_data.time
        lon = frame_data.longitude
        lat = frame_data.latitude
        Vmax = 0.0
        R35_4qAVG = 0.0
        MSLP = 0.0

        frame_info = pd.Series([region, ID, lon, lat, time, Vmax, R35_4qAVG, MSLP])
        frame_info.index = ['region', 'ID', 'lon', 'lat', 'time', 'Vmax', 'R35_4qAVG', 'MSLP']
        data_info.loc[data_info.shape[0]] = frame_info

        image = np.stack([
            frame_data.IR.values,
            frame_data.WV.values,
            np.zeros([201, 201]),
            np.zeros([201, 201])
        ], -1).astype('float32')
        data_matrix.append(image)

    data_info.time = pd.to_datetime(data_info.time, format='%Y%m%d%H%M')
    data_info.sort_values(['region', 'ID', 'time'], inplace=True)
    sorted_idx = np.array(data_info.index)
    data_info.reset_index(drop=True, inplace=True)

    all_data_matrix = np.stack(data_matrix)
    cropped_data_matrix = crop_center(all_data_matrix, 128)
    sorted_data_matrix = cropped_data_matrix[sorted_idx]

    image_matrix, label_df, feature_df = data_cleaning_and_organizing(sorted_data_matrix, data_info)
    return data_info, image_matrix, label_df, feature_df


def output_nc_files(output_folder, data_info, noon_image, pred_label):
    data_info['pred_Vmax'] = pred_label.numpy()
    for image, (index, label) in zip(noon_image, data_info.iterrows()):
        file_name = f'{label.ID}_{label.time.strftime("%Y%m%d%H%M")}.nc'
        out_path = os.path.join(output_folder, file_name)
        data_file = Dataset(out_path, "w", format="NETCDF4")

        data_file.createDimension('lat', size=64)
        data_file.createDimension('lon', size=64)
        data_file.createVariable('IR', 'f4', dimensions=('lat', 'lon'))
        data_file.createVariable('WV', 'f4', dimensions=('lat', 'lon'))
        data_file.createVariable('PMW_fake', 'f4', dimensions=('lat', 'lon'))
        data_file.createVariable('VIS_fake', 'f4', dimensions=('lat', 'lon'))
        data_file.variables['IR'][:] = image[:, :, 0]
        data_file.variables['WV'][:] = image[:, :, 1]
        data_file.variables['PMW_fake'][:] = image[:, :, 2]
        data_file.variables['VIS_fake'][:] = image[:, :, 3]

        data_file.region = label.region
        data_file.ID = label.ID
        data_file.time = label.time.strftime("%Y%m%d%H%M")
        data_file.longitude = label.lon
        data_file.latitude = label.lat
        data_file.pred_Vmax = label.pred_Vmax
        data_file.close()
        os.chmod(out_path, 0o666)

    csv_path = os.path.join(output_folder, 'output_pred.csv')
    data_info.drop(
        [
            'Vmax', 'R35_4qAVG', 'MSLP', 'region_code',
            'yday', 'yday_transform', 'yday_sin', 'yday_cos',
            'hour_transform', 'hour_sin', 'hour_cos', 'time'
        ], axis=1
    ).to_csv(csv_path, index=False)


def pred(experiment_path, experiment_stage, input_folder, output_folder):
    model = create_model_by_experiment_path_and_stage(experiment_path, experiment_stage)
    data_info, image_matrix, label_df, feature_df = load_input_nc_files(input_folder)
    preprocessed_image = crop_center(image_matrix, 64)
    noon_image = model.generate_noon_image(preprocessed_image, feature_df.to_numpy(dtype='float32'))
    pred_label = rotation_blending(model, 10, image_matrix, feature_df.to_numpy(dtype='float32'))
    output_nc_files(output_folder, data_info, noon_image, pred_label)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('experiment_path', help='name of the experiment setting, should match one of them file name in experiments folder')
    parser.add_argument('experiment_stage', help='name of the experiment setting, should match one of them file name in experiments folder')
    parser.add_argument('-i', '--input_nc_file_folder', default='sample_input')
    parser.add_argument('-o', '--output_folder', default='sample_output')
    args = parser.parse_args()
    pred(args.experiment_path, args.experiment_stage, args.input_nc_file_folder, args.output_folder)
