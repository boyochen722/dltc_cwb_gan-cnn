import tensorflow as tf
from modules.feature_generator import load_dataset
from modules.image_processor import image_augmentation, evenly_rotate


def get_sample_data(dataset, count):
    for batch_index, (images, feature, label) in dataset.enumerate():
        preprocessed_images = image_augmentation(images)
        sample_images = preprocessed_images[:count, ...]
        sample_feature = feature[:count, ...]
        sample_label = label[:count, ...]
        return sample_images, sample_feature, sample_label


def rotation_blending(model, blending_num, images, feature):
    evenly_rotated_images = evenly_rotate(images, blending_num)
    results = []
    for image in evenly_rotated_images:
        results.append(model(image, feature, training=False))
    return tf.reduce_mean(results, 0)


def evaluate_regression_loss(model, dataset, loss_function, blending_num=10):
    if loss_function == 'MSE':
        loss = tf.keras.losses.MeanSquaredError()
    elif loss_function == 'MAE':
        loss = tf.keras.losses.MeanAbsoluteError()

    avg_loss = tf.keras.metrics.Mean(dtype=tf.float32)
    for batch_index, (images, feature, label) in dataset.enumerate():
        pred = rotation_blending(model, blending_num, images, feature)
        batch_loss = loss(label, pred)
        avg_loss.update_state(batch_loss)
    return avg_loss.result()


def upsampling_good_quality_VIS_data(is_good_quality_VIS):
    good_quality_count = tf.reduce_sum(is_good_quality_VIS)
    total_count = tf.reduce_sum(tf.ones_like(is_good_quality_VIS))
    good_quality_rate = good_quality_count / total_count
    sample_weight_after_upsampling = is_good_quality_VIS / good_quality_rate
    return sample_weight_after_upsampling


def get_tensorflow_datasets(data_folder, batch_size, shuffle_buffer, label_column, good_VIS_only=False, coordinate='cart'):
    datasets = dict()
    for phase in ['train', 'valid', 'test']:
        phase_data = load_dataset(data_folder, phase, good_VIS_only, coordinate)
        images = tf.data.Dataset.from_tensor_slices(phase_data['image'])

        feature = tf.data.Dataset.from_tensor_slices(
            phase_data['feature'].to_numpy(dtype='float32')
        )

        label = tf.data.Dataset.from_tensor_slices(
            phase_data['label'][[label_column]].to_numpy(dtype='float32')
        )

        datasets[phase] = tf.data.Dataset.zip((images, feature, label)) \
            .shuffle(shuffle_buffer) \
            .batch(batch_size)

    return datasets
