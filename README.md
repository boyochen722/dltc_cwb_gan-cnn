# DLTC_GAN_CNN for CWB operation

docker repo: boyochen/dltc_cwb_gan-cnn

## Requirements

1. docker

## Compile

To compile the docker image:
```compile
sudo docker build -t dltc .

# if need proxy
sudo docker build \
--build-arg https_proxy=<proxy> \
-t dltc .
```

## Predict

To output the inensity estimation:
```
sudo docker run --rm \
-v ABSOLUTE_PATH_TO_INPUT_FOLDER:/data/input_files \
-v ABSOLUTE_PATH_TO_OUTPUT_FOLDER:/data/output_files \
--runtime=nvidia -e NVIDIA_VISIBLE_DEVICES=all \
dltc
```

## Input Format

![input](figs/input_format.png)

```
dimensions:
	lat = 201 ;
	lon = 201 ;

variables:
	float32 IR(lat, lon) ;
	float32 WV(lat, lon) ;

attributes:
	region: belongs to {WPAC, CPAC, EPAC, ATLN, IO, SH} ;
	ID: type = string, could be anything, just for identifying
	time: type = string, format = YYYYMMDDhhmm ;
	longitude: type = float ;
	latitude: type = float ;
```

## Output Format

![output](figs/output_format.png)

```
dimensions:
	lat = 64 ;
	lon = 64 ;

variables:
	float32 IR(lat, lon) ;
	float32 WV(lat, lon) ;
	float32 fake_PMW(lat, lon) ;
	float32 fake_VIS(lat, lon) ;

attributes:
	region: belongs to {WPAC, CPAC, EPAC, ATLN, IO, SH} ;
	ID: type = string, could be anything, just for identifying
	time: type = string, format = YYYYMMDDhhmm ;
	longitude: type = float ;
	latitude: type = float ;
	pred_Vmax: type = float ;
```
