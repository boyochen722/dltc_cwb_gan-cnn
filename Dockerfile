FROM tensorflow/tensorflow:latest-gpu

WORKDIR /app
ADD . /app

ENV MODEL_SETTING /app/experiments/production.yml
ENV INPUT_FOLDER /data/input_files
ENV OUTPUT_FOLDER /data/output_files

# default to use the first GPU
ENV CUDA_VISIBLE_DEVICES 0

# install python 3.7
RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt update
RUN apt install -y python3.7

# install pipenv
RUN pip install pipenv

# set up pipenv environment
RUN pipenv install
RUN pipenv run pip install tensorflow
RUN pipenv run pip install tensorflow_addons

CMD pipenv run python pred.py ${MODEL_SETTING} regressor_finetune_stage -i ${INPUT_FOLDER} -o ${OUTPUT_FOLDER}
